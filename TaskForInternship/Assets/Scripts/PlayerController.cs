using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class PlayerController : MonoBehaviour
{
    private int score = 0;
    public Text scoreText;

    public GameObject Player;
    public float speed;
    Ray ray;
    RaycastHit hit;
    Vector3 PlayerVr, Cursor;
    private void Start()
    {
        score = 0;
        scoreText.text = "Score: " + score.ToString(); 
    }

    void Update()
    {
        PlayerVr = new Vector3(Player.transform.position.x, 0.5f, Player.transform.position.z);
        Cursor = new Vector3(hit.point.x,0.5f, hit.point.z);
        speed = 10f;
#if (UNITY_ANDROID)
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Stationary || Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            Touch touch = Input.GetTouch(0);
            ray = Camera.main.ScreenPointToRay(touch.position);
            if (Physics.Raycast(ray, out hit))
            {
                Player.transform.position = Vector3.MoveTowards(PlayerVr, Cursor, speed);
            }
        }
#endif
#if (UNITY_EDITOR || UNITY_STANDALONE_WIN)
        if (Input.GetMouseButton(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                Player.transform.position = Vector3.MoveTowards(PlayerVr, Cursor, speed);
            }
        }
#endif
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Powerup"))
        {
            Destroy(other.gameObject);
            score++;
            scoreText.text = "Score: " + score.ToString();
        }
    }
}
