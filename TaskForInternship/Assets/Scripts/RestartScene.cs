using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartScene : MonoBehaviour
{
    public GameObject buttonRestart;
    private int countOfPowerup;

    // Start is called before the first frame update
    void Start()
    {
        countOfPowerup = GameObject.FindGameObjectsWithTag("Powerup").Length;
        Debug.Log(countOfPowerup);
    }

    // Update is called once per frame
    void Update()
    {
        CountOfPowerups();
        RestartGame();
    }
    void CountOfPowerups()
    {
        countOfPowerup = GameObject.FindGameObjectsWithTag("Powerup").Length;
        Debug.Log(countOfPowerup);
    }
    private void RestartGame()
    {
        if(countOfPowerup == 0)
        {
            buttonRestart.SetActive(true);
        }
    }
    public void ButtonDown()
    {
        SceneManager.LoadScene("Prototype");
    }

}
